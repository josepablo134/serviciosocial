# Arduino en parque
## Servicio Social 2019

# Clase Car

Esta clase encapsula todos los metodos
que controlan los leds y el puente H.

## Métodos de la clase

```cpp
void luzTrasera(unsigned char Izq, unsigned char Der);
void luzDelantera(unsigned char Izq,unsigned char Der);
void direccionalDerecha();
void direccionalIzquierda();
void luzFreno();

void avanza();
void reversa();
void frenar();
void girarDerecha();
void girarIzquierda();
void esperar( int tiempo=1000 );
```

## Ejemplo de uso

```cpp
#include "Car.hpp"

#define motorDerecho_A		2
#define motorDerecho_B		3
#define motorIzquierdo_A	5
#define motorIzquierdo_B	4
#define luzDelantera_Izq	7
#define luzDelantera_Der	8
#define luzTrasera_Izq		9
#define luzTrasera_Der		10

Car carrito( 	motorIzquierdo_A,
				motorIzquierdo_B,
				motorDerecho_A,
				motorDerecho_B,
				luzDelantera_Izq,
				luzDelantera_Der,
				luzTrasera_Izq,
				luzTrasera_Der
			);
void setup(){
	carrito.luzDelantera( HIGH , HIGH );
	carrito.luzTrasera( HIGH , HIGH );
	delay(250);
	carrito.luzDelantera( LOW , LOW );
	carrito.luzTrasera( LOW , LOW );
}

void loop(){
	carrito.frenar();
	delay(6000);
	carrito.avanza();
	carrito.esperar();
	
	carrito.girarDerecha();
	carrito.esperar();
	
	carrito.avanza();
	carrito.esperar();
	
	carrito.girarIzquierda();
	carrito.esperar();
	
	carrito.avanza();
	carrito.esperar();
	
	carrito.frenar();
	carrito.esperar();

	carrito.reversa();
	carrito.esperar();
}
```

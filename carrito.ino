#include "Car.hpp"

/// Conexión hacia el puente H
#define motorDerecho_A		2
#define motorDerecho_B		3
#define motorIzquierdo_A	4
#define motorIzquierdo_B	5

/// Conexión hacia los leds
#define luzDelantera_Izq	7
#define luzDelantera_Der	8
#define luzTrasera_Izq		9
#define luzTrasera_Der		10

Car carrito( 	motorIzquierdo_A,
				motorIzquierdo_B,
				motorDerecho_A,
				motorDerecho_B,
				luzDelantera_Izq,
				luzDelantera_Der,
				luzTrasera_Izq,
				luzTrasera_Der
			);
void setup(){
	carrito.luzDelantera( HIGH , HIGH );
	carrito.luzTrasera( HIGH , HIGH );
	delay(250);
	carrito.luzDelantera( LOW , LOW );
	carrito.luzTrasera( LOW , LOW );
}

void loop(){
	carrito.frenar();
	delay(6000);
	carrito.avanza();
	carrito.esperar();
	
	carrito.girarDerecha();
	carrito.esperar();
	
	carrito.avanza();
	carrito.esperar();
	
	carrito.girarIzquierda();
	carrito.esperar();
	
	carrito.avanza();
	carrito.esperar();
	
	carrito.frenar();
	carrito.esperar();

	carrito.reversa();
	carrito.esperar();
}

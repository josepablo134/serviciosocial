#include "Arduino.h"
#include "Car.hpp"

Car::Car(){
	MotorIzq_A = MotorIzq_B = \
	MotorDer_A = MotorDer_B = \
	LedFront_Izq = LedFront_Der = \
	LedBack_Izq = LedBack_Der = 0;
}

Car::Car( unsigned char MotorIzq_A,
		unsigned char MotorIzq_B, \
		unsigned char MotorDer_A, \
		unsigned char MotorDer_B, \
		unsigned char LedFront_Izq, \
		unsigned char LedFront_Der, \
		unsigned char LedBack_Izq, \
		unsigned char LedBack_Der)
{
	this->MotorIzq_A = MotorIzq_A;
	this->MotorIzq_B = MotorIzq_B;
	this->MotorDer_A = MotorDer_A;
	this->MotorDer_B = MotorDer_B;
	this->LedFront_Izq = LedFront_Izq;
	this->LedFront_Der = LedFront_Der;
	this->LedBack_Izq = LedBack_Izq;
	this->LedBack_Der = LedBack_Der;

	pinMode( this->MotorIzq_A , OUTPUT );
	pinMode( this->MotorIzq_B , OUTPUT );
	pinMode( this->MotorDer_A , OUTPUT );
	pinMode( this->MotorDer_B , OUTPUT );
	pinMode( this->LedBack_Izq , OUTPUT );
	pinMode( this->LedBack_Der , OUTPUT );
	pinMode( this->LedFront_Izq , OUTPUT );
	pinMode( this->LedFront_Der , OUTPUT );
}

/* Funciones de Control de Led's */
void Car::luzTrasera(unsigned char Izq, unsigned char Der){
	digitalWrite( this->LedBack_Izq , Izq );
	digitalWrite( this->LedBack_Der , Der );
}
void Car::luzDelantera(unsigned char Izq,unsigned char Der){
	digitalWrite( this->LedFront_Izq , Izq );
	digitalWrite( this->LedFront_Der , Der );
}
/*Funcion de control para direccionales*/
void Car::direccionalDerecha(){
		for (int i=0; i<10; i++){
		this->luzDelantera( LOW , LOW );
		this->luzTrasera( LOW , LOW );
		delay(100);
		this->luzDelantera( LOW , HIGH );
		this->luzTrasera( LOW , HIGH );
		delay(100);
	}
}
void Car::direccionalIzquierda(){
	for (int i=0; i<10; i++){
		this->luzDelantera( LOW , LOW );
		this->luzTrasera( LOW , LOW );
		delay(100);
		this->luzDelantera( HIGH , LOW );
		this->luzTrasera( HIGH , LOW );
		delay(100);
	}
}
void Car::luzFreno(){
	this->luzTrasera( HIGH, HIGH );
	delay(100);
	this->luzTrasera( LOW, LOW );
}

/* Funciones de Control de Movimientos*/
void Car::avanza(){
	digitalWrite( this->MotorIzq_A , HIGH );
	digitalWrite( this->MotorIzq_B , LOW );
	digitalWrite( this->MotorDer_A , HIGH );
	digitalWrite( this->MotorDer_B , LOW );
	this->luzDelantera( HIGH , HIGH );
}
void Car::reversa(){
	digitalWrite( this->MotorIzq_A , LOW );
	digitalWrite( this->MotorIzq_B , HIGH );
	digitalWrite( this->MotorDer_A , LOW );
	digitalWrite( this->MotorDer_B , HIGH );
	for(int i=0; i<10; i++){
		this->luzTrasera( LOW , LOW );
		delay(100);
		this->luzTrasera( HIGH , HIGH );
		delay(100);
	}
}
void Car::frenar(){
	this->luzFreno();
	delay(100);
	digitalWrite( this->MotorIzq_A , LOW );
	digitalWrite( this->MotorIzq_B , LOW );
	digitalWrite( this->MotorDer_A , LOW );
	digitalWrite( this->MotorDer_B , LOW );
}
void Car::girarDerecha(){
	this->direccionalDerecha();
	delay(100);
	this->frenar();
	digitalWrite( this->MotorIzq_A, HIGH);
	digitalWrite( this->MotorIzq_B, LOW);
}
void Car::girarIzquierda(){
	this->direccionalIzquierda();
	delay(100);
	this->frenar();
	digitalWrite( this->MotorDer_A , HIGH);
	digitalWrite( this->MotorDer_B , LOW);
}
void Car::esperar( int tiempo=1000 ){
	delay( tiempo );
}

#ifndef _CAR_INO_
#define _CAR_INO_

class Car{
private:
		unsigned char MotorIzq_A,MotorIzq_B;
		unsigned char MotorDer_A,MotorDer_B;
		unsigned char LedFront_Izq,LedFront_Der;
		unsigned char LedBack_Izq,LedBack_Der;
public:
	Car();
	Car( unsigned char MotorIzq_A,
		unsigned char MotorIzq_B, \
		unsigned char MotorDer_A, \
		unsigned char MotorDer_B, \
		unsigned char LedFront_Izq, \
		unsigned char LedFront_Der, \
		unsigned char LedBack_Izq, \
		unsigned char LedBack_Der);
/* Funciones de Control de Led's */
	void luzTrasera(unsigned char Izq, unsigned char Der);
	void luzDelantera(unsigned char Izq,unsigned char Der);
/*Funcion de control para direccionales*/
	void direccionalDerecha();
	void direccionalIzquierda();
	void luzFreno();

/* Funciones de Control de Movimientos*/
	void avanza();
	void reversa();
	void frenar();
	void girarDerecha();
	void girarIzquierda();
	void esperar( int tiempo=1000 );
};

#endif
